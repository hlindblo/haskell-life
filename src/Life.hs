{-# LANGUAGE TemplateHaskell #-}
module Life
  ( glider
  , neighbs
  , births
  , nextgen
  , Board
  , Game
  , step
  , makeGame
  , board
  , wrap
  ) where

import Control.Lens
import Linear.V2 (V2(..))

type Board = [V2 Integer]

data Game = Game
  { _generation :: Integer -- ^ current generation
  , _board :: Board
  } deriving (Show)

type GameState = State Game Board

makeLenses ''Game

-- initialize the game state. the value of any particular state is the
-- board configuration
makeGame :: Board -> Game
makeGame b = Game { _board = b, _generation = 1}
--
step :: Game -> Game
step = over board nextgen

isAlive :: Board -> V2 Integer -> Bool
isAlive b p = elem p b

isEmpty :: Board -> V2 Integer -> Bool
isEmpty b p = not $ isAlive b p

glider :: Board
glider = [V2 4 2, V2 2 3, V2 4 3, V2 3 4, V2 4 4]

neighbs :: V2 Integer -> [V2 Integer]
neighbs p = let dirs = [-1,0,1] in
  [wrap (p + (V2 x y)) | x <- dirs, y <- dirs, (x, y) /= (0, 0)]

wrap :: V2 Integer -> V2 Integer
wrap p = flip mod 16 <$> p

liveneighbs :: Board -> V2 Integer -> Int
liveneighbs b = length . filter (isAlive b) . neighbs

survivors :: Board -> [V2 Integer]
survivors b = [ p | p <- b, elem (liveneighbs b p) [2,3]]

births :: Board -> [V2 Integer]
births b = [ p | p <- rmdups (concat (map neighbs b)), isEmpty b p, (liveneighbs b p) == 3]

rmdups :: Eq a => [a] -> [a]
rmdups [] = []
rmdups (x:xs) = x : rmdups (filter (/= x) xs)

nextgen :: Board -> Board
nextgen b = survivors b ++ births b
