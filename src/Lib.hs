{-# LANGUAGE TemplateHaskell #-}
module Lib
    ( someFunc
    ) where
import Control.Monad (void)
import Life

import qualified Brick as B
import Data.Monoid
import Control.Lens
import Control.Monad.State
import Brick.Types
import Linear.V2
import Brick.Widgets.Core
  ( (<=>)
  , (<+>)
  , vLimit
  , hLimit
  , hBox
  , updateAttrMap
  , withBorderStyle
  , txt
  , str
  )
import Brick.Util (fg, bg, on, clamp)
import Brick.Widgets.Center
import Brick.Widgets.Border
import Brick.Widgets.Border.Style
import qualified Graphics.Vty as V

boardSize :: Integer
boardSize = 15

someFunc :: IO ()
someFunc = void $ B.defaultMain app (makeGame Life.glider)

type Tick = ()
type Name = ()

theBaseAttr :: B.AttrName
theBaseAttr = B.attrName "theBase"

xDoneAttr, xToDoAttr :: B.AttrName
xDoneAttr = theBaseAttr <> B.attrName "X:done"
xToDoAttr = theBaseAttr <> B.attrName "X:remaining"

yDoneAttr, yToDoAttr :: B.AttrName
yDoneAttr = theBaseAttr <> B.attrName "Y:done"
yToDoAttr = theBaseAttr <> B.attrName "Y:remaining"

zDoneAttr, zToDoAttr :: B.AttrName
zDoneAttr = theBaseAttr <> B.attrName "Z:done"
zToDoAttr = theBaseAttr <> B.attrName "Z:remaining"

aliveAttr, emptyAttr :: B.AttrName
aliveAttr = theBaseAttr <> B.attrName "aliveAttr"
emptyAttr = theBaseAttr <> B.attrName "emptyAttr"

theMap :: B.AttrMap
theMap = B.attrMap V.defAttr
         [ (theBaseAttr,               bg V.brightBlack)
         , (xDoneAttr,                 V.black `on` V.white)
         , (xToDoAttr,                 V.white `on` V.black)
         , (yDoneAttr,                 V.magenta `on` V.yellow)
         , (zDoneAttr,                 V.blue `on` V.green)
         , (zToDoAttr,                 V.blue `on` V.red)
         , (aliveAttr,                 V.blue `on` V.blue)
         ]

app :: B.App Game Tick Name
app = B.App { B.appDraw = drawUI
          , B.appChooseCursor = B.neverShowCursor
          , B.appHandleEvent = handleEvent
          , B.appStartEvent = return
          , B.appAttrMap = const theMap

          }

-- A square widget
cw :: Widget Name
cw = str "  "

drawUI :: Game -> [B.Widget Name]
drawUI g = [ui]
  where
    ui =
      withBorderStyle unicode $
      borderWithLabel (str "Hello!") $
      B.vBox rows
      where
        rows = [B.hBox $ cellsInRow r | r <- [15,14..0]]
        cellsInRow y = [drawCoord (V2 x y) | x <- [0..15]]
        drawCoord  = drawCell . cellAt
        cellAt c
          | c `elem` (g ^. Life.board) = Alive
          | otherwise = Dead


data Cell = Alive | Dead

drawCell :: Cell -> B.Widget Name
drawCell Alive = B.withAttr aliveAttr cw
drawCell Dead = B.withAttr emptyAttr cw
handleEvent :: Game -> B.BrickEvent Name Tick -> B.EventM Name (B.Next Game)
handleEvent g (B.VtyEvent (V.EvKey (V.KChar 'n') [])) = B.continue $ step g
